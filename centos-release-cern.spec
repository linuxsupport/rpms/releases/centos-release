%define debug_package %{nil}
%define product_family CentOS Linux
%define variant_titlecase Server
%define variant_lowercase server
%ifarch %{arm}
%define release_name AltArch
%define contentdir   altarch
%else
%define release_name Core
%define contentdir   centos
%endif
%ifarch ppc64le
%define tuned_profile :server
%endif
%define infra_var stock
#CERN dnf var
%define cernbase 8
%define base_release_version 8
%define full_release_version 8
%define dist_release_version 8
%define upstream_rel_long 8.2-0
%define upstream_rel 8.2
%define centos_rel 2.2004
#define beta Beta
%define dist .el%{dist_release_version}.cern

# The anaconda scripts in %%{_libexecdir} can create false requirements
%global __requires_exclude_from %{_libexecdir}

Name:           centos-release
Version:        %{upstream_rel}
Release:        %{centos_rel}.0.2.2%{?dist}
Epoch:          666
Summary:        %{product_family} release file
Group:          System Environment/Base
License:        GPLv2
%ifnarch %{arm}
%define pkg_name %{name}
%else
%define pkg_name centos-userland-release
%package -n %{pkg_name}
Summary:        %{product_family} release file
%endif
Provides:       centos-release = %{version}-%{release}
Provides:       centos-release(upstream) = %{upstream_rel}
Provides:       redhat-release = %{upstream_rel_long}
Provides:       system-release = %{upstream_rel_long}
Provides:       system-release(releasever) = %{base_release_version}
Provides:       base-module(platform:el%{base_release_version})

Provides:       centos-release-eula
Provides:       redhat-release-eula

Requires:       centos-repos(%{base_release_version})

Source1:        85-display-manager.preset
Source2:        90-default.preset
Source3:        99-default-disable.preset
Source10:       RPM-GPG-KEY-centosofficial
Source11:       RPM-GPG-KEY-centostesting

Source100:      rootfs-expand

Source200:      EULA
Source201:      GPL
Source202:      Contributors

Source300:      CentOS-Base.repo
Source301:      CentOS-CR.repo
Source302:      CentOS-Debuginfo.repo
Source303:      CentOS-Extras.repo
Source304:      CentOS-fasttrack.repo
Source305:      CentOS-Media.repo
Source306:      CentOS-Sources.repo
Source307:      CentOS-Vault.repo
Source308:      CentOS-AppStream.repo
Source309:      CentOS-PowerTools.repo
Source310:      CentOS-centosplus.repo
Source311:      CentOS-HA.repo
Source312:      CentOS-Devel.repo

#CERN
Source400:      CentOS-CERN.repo
Source402:      RPM-GPG-KEY-EUGridPMA-RPM-3
Source403:      RPM-GPG-KEY-koji
Source404:      RPM-GPG-KEY-kojiv2
Source405:      RPM-GPG-KEY-ai

%ifarch %{arm}
%description -n %{pkg_name}
%{product_family} release files
%endif

%description
%{product_family} release files

%package -n centos-repos
Summary:        %{product_family} package repositories
Group:          System Environment/Base
Provides:       centos-repos(%{base_release_version}) = %{upstream_rel}
Requires:       system-release = %{upstream_rel}
Requires:       centos-gpg-keys
Conflicts:      %{name} < 8.0-0.1905.0.10

%description -n centos-repos
%{product_family} package repository files for yum and dnf

%package -n centos-gpg-keys
Summary:        %{product_family} RPM keys
Group:          System Environment/Base
Conflicts:      %{name} < 8.0-0.1905.0.10
BuildArch:      noarch

%description -n centos-gpg-keys
%{product_family} RPM signature keys

%prep
echo OK

%build
echo OK

%install
rm -rf %{buildroot}

# create skeleton
mkdir -p %{buildroot}/etc
mkdir -p %{buildroot}%{_prefix}/lib

# create /etc/system-release and /etc/redhat-release
echo "%{product_family} release %{full_release_version}.%{centos_rel} (%{release_name}) " > %{buildroot}/etc/centos-release
echo "Derived from Red Hat Enterprise Linux %{upstream_rel} (Source)" > %{buildroot}/etc/centos-release-upstream
ln -s centos-release %{buildroot}/etc/system-release
ln -s centos-release %{buildroot}/etc/redhat-release

# Create the os-release file
cat << EOF >>%{buildroot}%{_prefix}/lib/os-release
NAME="%{product_family}"
VERSION="%{full_release_version} (%{release_name})"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="%{full_release_version}"
PLATFORM_ID="platform:el%{base_release_version}"
PRETTY_NAME="%{product_family} %{full_release_version} (%{release_name})"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:centos:centos:%{base_release_version}%{?tuned_profile}"
HOME_URL="https://www.centos.org/"
BUG_REPORT_URL="https://bugs.centos.org/"

CENTOS_MANTISBT_PROJECT="CentOS-%{base_release_version}"
CENTOS_MANTISBT_PROJECT_VERSION="%{base_release_version}"
REDHAT_SUPPORT_PRODUCT="centos"
REDHAT_SUPPORT_PRODUCT_VERSION="%{base_release_version}"

EOF
# Create the symlink for /etc/os-release
ln -s ../usr/lib/os-release %{buildroot}%{_sysconfdir}/os-release

# write cpe to /etc/system/release-cpe
echo "cpe:/o:centos:centos:%{base_release_version}" > %{buildroot}/etc/system-release-cpe

# create /etc/issue and /etc/issue.net
echo '\S' > %{buildroot}/etc/issue
echo 'Kernel \r on an \m' >> %{buildroot}/etc/issue
cp %{buildroot}/etc/issue %{buildroot}/etc/issue.net
echo >> %{buildroot}/etc/issue

# copy GPG keys
mkdir -p -m 755 %{buildroot}/etc/pki/rpm-gpg
install -m 644 %{SOURCE10} %{buildroot}/etc/pki/rpm-gpg
install -m 644 %{SOURCE11} %{buildroot}/etc/pki/rpm-gpg

#CERN
install -m 644 %{SOURCE402} %{buildroot}/etc/pki/rpm-gpg
install -m 644 %{SOURCE403} %{buildroot}/etc/pki/rpm-gpg
install -m 644 %{SOURCE404} %{buildroot}/etc/pki/rpm-gpg
install -m 644 %{SOURCE405} %{buildroot}/etc/pki/rpm-gpg

# copy yum repos
mkdir -p -m 755 %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE300} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE301} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE302} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE303} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE304} %{buildroot}/etc/yum.repos.d
# CERN doesn't care about the Media repo or the Vault repo
#install -m 644 %{SOURCE305} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE306} %{buildroot}/etc/yum.repos.d
#install -m 644 %{SOURCE307} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE308} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE309} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE310} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE311} %{buildroot}/etc/yum.repos.d
install -m 644 %{SOURCE312} %{buildroot}/etc/yum.repos.d
# CERN
install -m 644 %{SOURCE400} %{buildroot}/etc/yum.repos.d

mkdir -p -m 755 %{buildroot}/etc/dnf/vars
echo "%{infra_var}" > %{buildroot}/etc/dnf/vars/infra
echo "%{contentdir}" >%{buildroot}/etc/dnf/vars/contentdir
#CERN
echo "%{cernbase}" >%{buildroot}/etc/dnf/vars/cernbase
%ifarch %{arm}
echo %{base_release_version} > %{buildroot}/etc/dnf/vars/releasever
%endif

# set up the dist tag macros
install -d -m 755 %{buildroot}/etc/rpm
cat >> %{buildroot}/etc/rpm/macros.dist << EOF
# dist macros.

%%centos_ver %{base_release_version}
%%centos %{base_release_version}
%%rhel %{base_release_version}
%%dist .el%{base_release_version}
%%el%{base_release_version} 1
EOF

# use unbranded datadir
mkdir -p -m 755 %{buildroot}/%{_datadir}/centos-release
ln -s centos-release %{buildroot}/%{_datadir}/redhat-release
install -m 644 %{SOURCE200} %{buildroot}/%{_datadir}/centos-release

# use unbranded docdir
mkdir -p -m 755 %{buildroot}/%{_docdir}/centos-release
ln -s centos-release %{buildroot}/%{_docdir}/redhat-release
install -m 644 %{SOURCE201} %{buildroot}/%{_docdir}/centos-release
install -m 644 %{SOURCE202} %{buildroot}/%{_docdir}/centos-release

# copy systemd presets
mkdir -p %{buildroot}/%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE1} %{buildroot}/%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE2} %{buildroot}/%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE3} %{buildroot}/%{_prefix}/lib/systemd/system-preset/

%ifarch %{arm} aarch64
# Install armhfp/aarch64 specific tools
install -D -m 0755 %{SOURCE100} %{buildroot}%{_bindir}/rootfs-expand
%endif


%clean
rm -rf %{buildroot}

%files -n %{pkg_name}
%defattr(0644,root,root,0755)
/etc/redhat-release
/etc/system-release
/etc/centos-release
/etc/centos-release-upstream
%config(noreplace) /etc/os-release
%config /etc/system-release-cpe
%config(noreplace) /etc/issue
%config(noreplace) /etc/issue.net
/etc/rpm/macros.dist
%{_docdir}/redhat-release
%{_docdir}/centos-release/*
%{_datadir}/redhat-release
%{_datadir}/centos-release/*
%{_prefix}/lib/os-release
%{_prefix}/lib/systemd/system-preset/*
%ifarch %{arm} aarch64
%attr(0755,root,root) %{_bindir}/rootfs-expand
%endif

%files -n centos-repos
%config(noreplace) /etc/yum.repos.d/*
%config(noreplace) /etc/dnf/vars/*

%files -n centos-gpg-keys
/etc/pki/rpm-gpg/

%changelog
* Thu Oct 15 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 8.2-2.2004.0.2.2
- Version bump

* Mon Sep 21 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 8.2-2.2004.0.2
- CERN rebuild

* Mon Sep 14 2020 Carl George <carl@george.computer> - 8.2-2.2004.0.2
- Require centos-repos(8) virtual provides to facilitate future upgrade paths
- Remove version restriction on centos-gpg-keys requirement

* Tue Jun 16 2020 Alex Iribarren <Alex.Iribarren@cern.ch> - 8-2.0.1
- Rebase to 8.2 release and readd CERN patches

* Fri May 15 2020 Pablo Greco <pgreco@centosproject.org> - 8-2.0.1
- Relax dependency for centos-repos
- Remove update_boot, it was never used in 8
- Add rootfs_expand to aarch64
- Bump release for 8.2

* Wed Mar 18 2020 Ben Morrice <ben.morrice@cern.ch> - 8.1.0.10
- rebase release

* Thu Mar 12 2020 bstinson@centosproject.org - 8-1.0.9
- Add the Devel repo to centos-release
- Install os-release(5) content to /usr/lib and have /etc/os-release be a symlink (ngompa)pr#9

* Thu Jan 16 2020 Ben Morrice <ben.morrice@cern.ch> - 8-1.0.9
- rebase to 8.1 release and readd CERN patches

* Thu Jan 02 2020 Brian Stinson <bstinson@centosproject.org> - 8-1.0.8
- Add base module platform Provides so DNF can auto-discover modular platform (ngompa)pr#6
- Switched CR repo to mirrorlist to spread the load (arrfab)pr#5

* Thu Dec 19 2019 bstinson@centosproject.org - 8-1.0.7
- Typo fixes
- Disable the HA repo by default

* Wed Dec 18 2019 Pablo Greco <pgreco@centosproject.org> - 8-1.el8
- Fix requires in armhfp

* Tue Dec 17 2019 bstinson@centosproject.org - 8-1.el8
- Add the HighAvailability repository

* Wed Aug 14 2019 Neal Gompa <ngompa@centosproject.org> 8-1.el8
- Split repositories and GPG keys out into subpackages

* Sat Aug 10 2019 Fabian Arrotin <arrfab@centos.org> 8-0.el8
- modified baseurl paths, even if disabled

* Sat Aug 10 2019 Fabian Arrotin <arrfab@centos.org> 8-0.el8
- Enabled Extras by default.
- Fixed sources paths for BaseOS/AppStream

* Sat Aug 10 2019 Brian Stinson <bstinson@centosproject.org> 8-0.el7
- Update Debuginfo and fasttrack to use releasever
- Fix CentOS-media.repo to include appstream

* Wed May  8 2019 Pablo Greco <pablo@fliagreco.com.ar> 8-0.el7
- Initial setup for CentOS-8

