# centos-release

NOTE: This repo provided `centos-release` for CentOS 8.0,8.1 and 8.2.

Current and future CentOS Linux releases will use the RPMs in [centos-linux-release](https://gitlab.cern.ch/linuxsupport/rpms/releases/centos-linux-release) and [centos-repos](https://gitlab.cern.ch/linuxsupport/rpms/centos-repos) repositories.


# DEPRECATED

Starting in CentOS 8.3, this RPM was renamed. The new version is here:

https://gitlab.cern.ch/linuxsupport/rpms/releases/centos-linux-release
